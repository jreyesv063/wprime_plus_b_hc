import awkward as ak
import numpy as np
from coffea.analysis_tools import Weights
from wprime_plus_b.corrections.btag import BTagCorrector
from wprime_plus_b.corrections.pileup import add_pileup_weight
from wprime_plus_b.corrections.pujetid import add_pujetid_weight
from wprime_plus_b.corrections.lepton import ElectronCorrector, MuonCorrector
from wprime_plus_b.corrections.tau import TauCorrector


def add_l1prefiring_sf(events, weights_container, year="2017", variation="nominal"):
    # add L1prefiring weights
    if year in ("2016", "2017"):
        if variation == "nominal":
            weights_container.add(
                "L1Prefiring",
                weight=events.L1PreFiringWeight.Nom,
                weightUp=events.L1PreFiringWeight.Up,
                weightDown=events.L1PreFiringWeight.Dn,
            )
        else:
            weights_container.add(
                "L1Prefiring",
                weight=events.L1PreFiringWeight.Nom,
            )


def add_pileup_sf(
    events, weights_container, year="2017", yearmod="", variation="nominal"
):
    add_pileup_weight(
        events,
        weights_container=weights_container,
        year=year,
        year_mod=yearmod,
        variation=variation,
    )


def add_pujetid_sf(
    jets,
    weights_container,
    year="2017",
    yearmod="",
    wp = "T",
    variation="nominal",
):

    add_pujetid_weight(
        jets=jets,
        weights=weights_container,
        year=year,
        year_mod=yearmod,
        working_point=wp,
        variation=variation,
    )


def add_btag_sf(
    jets,
    btag_wp,
    weights_container,
    year="2017",
    yearmod="",
    variation="nominal",
):
    

    # b-tagging corrector
    btag_corrector = BTagCorrector(
        jets=jets,
        weights=weights_container,
        sf_type="comb",
        worging_point=btag_wp,
        tagger="deepJet",
        year=year,
        year_mod=yearmod,
        full_run=False,
        variation=variation,
    )
    # add b-tagging weights
    btag_corrector.add_btag_weights(flavor="bc")


def add_lepton_sf(
    events,
    weights_container,
    year="2017",
    yearmod="",
    variation="nominal",
    lepton_flavor = "mu"
):
    electron_corrector = ElectronCorrector(
        electrons=events.Electron,
        weights=weights_container,
        year=year,
        year_mod=yearmod,
        variation=variation,
    )
    # add electron ID weights
    electron_corrector.add_id_weight(
        id_working_point="wp90iso"
    )
    # add electron reco weights
    electron_corrector.add_reco_weight()

    # muon corrector
    muon_corrector = MuonCorrector(
        muons=events.Muon,
        weights=weights_container,
        year=year,
        year_mod=yearmod,
        variation=variation,
        id_wp="tight",
        iso_wp="tight",
    )
    # add muon ID weights
    muon_corrector.add_id_weight()

    # add muon iso weights
    muon_corrector.add_iso_weight()
    
    if lepton_flavor == "mu":
        muon_corrector.add_triggeriso_weight()



        
    
def add_tau_sf(taus_corrected,
                tau_selection,
                weights_container,
                events,
                year="2017",
                yearmod="",
                variation="nominal",
                lepton_flavor = "tau",
                channel = "Ztautau",
):
    
    tau_corrector = TauCorrector(
                        taus = taus_corrected, 
                        weights=weights_container, 
                        year=year, 
                        year_mod="", 
                        tag="tau", 
                        jetVsTau = tau_selection[lepton_flavor]["tau_Jet"],
                        eVsTau = tau_selection[lepton_flavor]["tau_e"],
                        muVsTau = tau_selection[lepton_flavor]["tau_mu"],
                        variation="nom"
    )    
    
    # ----------------------------------------- #
    # --- This is the DM and PT SF ------------ #
    # By default, use the pT-dependent SFs
    # Be carefull: tauVse has to be Tight or VVLoose, and TauVsJet: Loose, Medium, Tight, VTigh
    tau_corrector.add_id_weight_DeepTau2017v2p1VSjet("dm")
    

    # mu -> tau_h fake rate SFs
    # Be carefull: tauVsmu has to be Loose, Medium, Tight, VLoose      
    tau_corrector.add_id_weight_DeepTau2017v2p1VSmu()


    # e -> tau_h fake rate SFs
    # Be carefull: tauVse has to be Loose, Medium, Tight, VLoose, VTight, VVLoose, VVTight      
    tau_corrector.add_id_weight_DeepTau2017v2p1VSe()
    
   
    # Z-> tau tau use a ditau trigger
    if channel  == "Ztautau":
        if lepton_flavor == "tau":
            
            trigger_name = "DoubleMediumChargedIsoPFTau35_Trk1_eta2p1_Reg"
            mask_trigger = (events.HLT[trigger_name])
            
            
            tau_corrector.add_id_weight_diTauTrigger(mask_trigger, 'ditau', 'sf')
    
   

def event_weights(
    events,
    jets,
    lepton_flavor,
    channel,
    btag_wp,
    taus_corrected,
    tau_selection,
    is_mc=True,
    year="2017",
    yearmod="",
    variation="nominal",
):
    weights_container = Weights(len(events), storeIndividual=True)
    if is_mc:
        # add gen weigths
        #weights_container.add("genweight", events.genWeight)
        # Evalúa la función lambda en el contexto de tus datos para obtener los valores deseados y añade los valores calculados al contenedor de pesos
        genweight_values = lambda events: np.where(events.genWeight > 0, 1, -1)
        weights_container.add("genweight", genweight_values(events))


        # add l1prefiring weigths
        add_l1prefiring_sf(events, weights_container, year, variation)

        # add pileup weigths
        add_pileup_sf(events, weights_container, year, yearmod, variation)

        # add pujetid weigths
        add_pujetid_sf(
            jets,
            weights_container,
            year,
            yearmod,
            btag_wp,
            variation,
        )
        
        # add b-tagging weigths
        add_btag_sf(
            jets,
            btag_wp,
            weights_container,
            year,
            yearmod,
            variation,
        )
        
        # add lepton weights
        add_lepton_sf(events,weights_container,year,yearmod,variation, lepton_flavor)
        
        # add tau weights
        add_tau_sf(taus_corrected, tau_selection, weights_container, events, year, yearmod, variation, lepton_flavor, channel)
        

    return weights_container