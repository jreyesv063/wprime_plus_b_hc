import awkward as ak
from coffea.analysis_tools import Weights
from wprime_plus_b.corrections.btag import BTagCorrector
from wprime_plus_b.corrections.pileup import add_pileup_weight
from wprime_plus_b.corrections.pujetid import add_pujetid_weight
from wprime_plus_b.corrections.lepton import ElectronCorrector, MuonCorrector
from wprime_plus_b.corrections.tau import TauCorrector


def add_l1prefiring_sf(events, weights_container, year="2017", variation="nominal"):
    # add L1prefiring weights
    if year in ("2016", "2017"):
        if variation == "nominal":
            weights_container.add(
                "L1Prefiring",
                weight=events.L1PreFiringWeight.Nom,
                weightUp=events.L1PreFiringWeight.Up,
                weightDown=events.L1PreFiringWeight.Dn,
            )
        else:
            weights_container.add(
                "L1Prefiring",
                weight=events.L1PreFiringWeight.Nom,
            )


def add_pileup_sf(
    events, weights_container, year="2017", yearmod="", variation="nominal"
):
    add_pileup_weight(
        events,
        weights_container=weights_container,
        year=year,
        year_mod=yearmod,
        variation=variation,
    )


def add_pujetid_sf(
    jets,
    weights_container,
    year="2017",
    yearmod="",
    variation="nominal",
):
    add_pujetid_weight(
        jets=jets,
        weights=weights_container,
        year=year,
        year_mod=yearmod,
        working_point="T",
        variation=variation,
    )


def add_btag_sf(
    jets,
    weights_container,
    year="2017",
    yearmod="",
    variation="nominal",
):
    # b-tagging corrector
    btag_corrector = BTagCorrector(
        jets=jets,
        weights=weights_container,
        sf_type="comb",
        worging_point="M",
        tagger="deepJet",
        year=year,
        year_mod=yearmod,
        full_run=False,
        variation=variation,
    )
    # add b-tagging weights
    btag_corrector.add_btag_weights(flavor="bc")
    #btag_corrector.add_btag_weights(flavor="light")

def add_lepton_sf(
    events,
    weights_container,
    year="2017",
    yearmod="",
    variation="nominal",
):
    electron_corrector = ElectronCorrector(
        electrons=events.Electron,
        weights=weights_container,
        year=year,
        year_mod=yearmod,
        variation=variation,
    )
    # add electron ID weights
    electron_corrector.add_id_weight(
        id_working_point="wp90iso"
    )
    # add electron reco weights
    electron_corrector.add_reco_weight()

    # muon corrector
    muon_corrector = MuonCorrector(
        muons=events.Muon,
        weights=weights_container,
        year=year,
        year_mod=yearmod,
        variation=variation,
        id_wp="tight",
        iso_wp="tight",
    )
    # add muon ID weights
    muon_corrector.add_id_weight()

    # add muon iso weights
    muon_corrector.add_iso_weight()

    
def add_tau_sf(taus_corrected,
               tau_selection,
                weights_container,
                year="2017",
                yearmod="",
                variation="nominal",
):
    
    tau_corrector = TauCorrector(
                        taus = taus_corrected, 
                        weights=weights_container, 
                        year="2017", 
                        year_mod="", 
                        tag="tau", 
                        jetVsTau =  tau_selection["tau_Jet"],
                        eVsTau =  tau_selection["tau_e"],
                        muVsTau =  tau_selection["tau_mu"],
                        variation="nom"
    )    
    
    # By default, use the pT-dependent SFs
    # Be carefull: tauVse has to be Tight or VVLoose, and TauVsJet: Loose, Medium, Tight, VTigh
    test_jet = tau_corrector.add_id_weight_DeepTau2017v2p1VSjet()

    # mu -> tau_h fake rate SFs
    # Be carefull: tauVsmu has to be Loose, Medium, Tight, VLoose      
    test_mu =tau_corrector.add_id_weight_DeepTau2017v2p1VSmu()

    # e -> tau_h fake rate SFs
    # Be carefull: tauVse has to be Loose, Medium, Tight, VLoose, VTight, VVLoose, VVTight      
    test_e = tau_corrector.add_id_weight_DeepTau2017v2p1VSjet("pt")
    
    # Ojo, solo para el Z-> tau tau
    test_trigger = tau_corrector.add_id_weight_diTauTrigger('ditau', 'sf')

def event_weights(
    events,
    jets,
    taus_corrected,
    tau_selection,
    is_mc=True,
    year="2017",
    yearmod="",
    variation="nominal",
):
    weights_container = Weights(len(events), storeIndividual=True)
    if is_mc:
        # add gen weigths
        weights_container.add("genweight", events.genWeight)

        # add l1prefiring weigths
        add_l1prefiring_sf(events, weights_container, year, variation)

        # add pileup weigths
        add_pileup_sf(events, weights_container, year, yearmod, variation)

        # add pujetid weigths
        add_pujetid_sf(
            jets,
            weights_container,
            year,
            yearmod,
            variation,
        )
        
        # add b-tagging weigths
        add_btag_sf(
            jets,
            weights_container,
            year,
            yearmod,
            variation,
        )
        
        # add lepton weights
        add_lepton_sf(events,weights_container,year,yearmod,variation)
        
        # add tau weights
        add_tau_sf(taus_corrected, tau_selection, weights_container,year,yearmod,variation)
        

    return weights_container