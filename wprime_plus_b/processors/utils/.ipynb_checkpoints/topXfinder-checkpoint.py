import numpy as np
import awkward as ak
import json
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema
from wprime_plus_b.processors.utils.analysis_utils import chi2_test, delta_r_mask, pdg_masses, tagger_constants

# --------------------------
# Top tagger
# --------------------------

class topXfinder:
    def __init__(
        self,
        year: str = "2017",
        year_mod: str = "",
    ) -> None:
        
        
        # year
        self.year = year
        self.year_mod = year_mod        

        self.top_mass_pdg, self.w_mass_pdg = pdg_masses()
    
    def pdg_masses(self):
        
        return self.top_mass_pdg, self.w_mass_pdg
        
    ######################################
    ######### Initial scenario ###########
    ######################################
    
    def Scenario_initial(self, bjets, jets):
        
        top_sigma, top_low_mass, top_up_mass, w_sigma, w_low_mass, w_up_mass, chi2 = tagger_constants("hadronic")
            
        
        jet_b = bjets


        jet_j1 = ak.pad_none(jets, 2)[:, 0]
        jet_j2 = ak.pad_none(jets, 2)[:, 1] 

        # Cross cleaning jet_1 and jet_2
        cross_cleaning = (
            (jet_j1.delta_r(jet_j2) > 0.4)
        )

        b1_cc = jet_b.mask[cross_cleaning]            

        j1_cc = jet_j1.mask[cross_cleaning]            
        j2_cc = jet_j2.mask[cross_cleaning]               


        # ---------------------------------------
        # We will define the W using 2j
        # ---------------------------------------
        dijet = j1_cc + j1_cc

        good_w_mass = (
            (dijet.mass > w_low_mass) 
            & (dijet.mass < w_up_mass) 
        )    

        w = dijet.mask[good_w_mass] 

        # ---------------------------------------
        # We will define the top using the b + 2j
        # ---------------------------------------
        # Suponemos que el top se reconstruye con el primer b
        trijet = w + b1_cc


        good_tops = (
            (trijet.mass > top_low_mass)
            & (trijet.mass < top_up_mass)
        )      


        top = trijet.mask[good_tops] 

        # ---------------------------------------
        # chi2 criteria
        # ---------------------------------------
        chi2_cal = chi2_test(top, w,  top_sigma, w_sigma, self.top_mass_pdg, self.w_mass_pdg)

        good_chi2 = ak.firsts(chi2_cal) < chi2
        
        
        
        
        tops = top.mask[good_chi2]
        top_mass = ak.fill_none(tops.mass, 0)
        ws = w.mask[good_chi2]
        w_mass = ak.fill_none(ws.mass, 0)
        mask = ak.fill_none(good_chi2, False)
        
        
        return tops, ws, mask
    
    
    
    ######################################
    #########  2 Jet #####################
    ######################################
        
    def Scenario_2jets(self, bjets, tops):
        
        jet_top = tops.mask[ak.num(tops) == 1]
        jet_b = bjets.mask[ak.num(bjets) == 1]
          
        good_top_mass = (
            (jet_top.mass > 100) 
            & (jet_top.mass < 400) 
        )    


        top_scenario_2j = jet_top.mask[good_top_mass]
        top_2j_mass = ak.fill_none(top_scenario_2j.mass, 0)
        mask_scenario_2j = ak.fill_none(ak.firsts(good_top_mass), False)
        

        #print("Top tagger_mask_2j (True): ", ak.sum(mask_scenario_2j), mask_scenario_2j.ndim, mask_scenario_2j[mask_scenario_2j == True])
        
        return top_scenario_2j, mask_scenario_2j, top_2j_mass, ak.sum(mask_scenario_2j)      
    
    
    
    ######################################
    #########  3 Jet #####################
    ######################################
        
    # ----------------------------------------
    #  Scenario V (W = 1  +  b = 2)
    # ----------------------------------------
    def Scenario_3jets(self, bjets, wjets):
        
        jet_b = bjets.mask[ak.num(bjets) == 2]
        jet_w = wjets.mask[ak.num(wjets) == 1]


        jet_b1 = ak.pad_none(jet_b, 2)[:, 0]
        jet_b2 = ak.pad_none(jet_b, 2)[:, 1]

        # Cross cleaning bjets
        cross_cleaning = (jet_b2.delta_r(jet_b1) > 0.4)

        b1_cc = jet_b1.mask[cross_cleaning]            
        b2_cc = jet_b2.mask[cross_cleaning]


        good_w_mass = (
            (jet_w.mass > 40) 
            & (jet_w.mass < 200) 
        )

        w = jet_w[good_w_mass]


        # ---------------------------------------
        # We will define the top using the b + W
        # ---------------------------------------
        condition = (
            ((w + b1_cc).mass > 100) 
            & ((w + b1_cc).mass < 300)
        )

        dijet = ak.where(condition,
                       w + b1_cc,
                       w + b2_cc)


        good_tops = (
                (dijet.mass > 100)
                & (dijet.mass < 300)
        )      

        top = dijet.mask[good_tops]



        # ---------------------------------------
        # chi2 criteria
        # ---------------------------------------
        chi2_cal = chi2_test(top, w, 37.14, 20.09, 173.1, 80.4)

        good_chi2 = (chi2_cal < 5)


        top_scenario_3j = top.mask[good_chi2]
        top_3j_mass = ak.fill_none(top_scenario_3j.mass, 0)
        mask_scenario_3j = ak.fill_none(ak.firsts(good_chi2), False)    


        #print("Top tagger_mask_3j (True): ", ak.sum(mask_scenario_3j), mask_scenario_3j.ndim, mask_scenario_V[mask_scenario_3j == True])    

        return top_scenario_3j, mask_scenario_3j, top_3j_mass, ak.sum(mask_scenario_3j)
    
    
    
    
    
    ######################################
    #########  4 Jet #####################
    ######################################
        
    # ----------------------------------------
    #  Scenario IX (b=2  + light_jets = 2)
    # ----------------------------------------        
    def Scenario_4jets(self, bjets, jets):

        b_jets = bjets.mask[ak.num(bjets) == 2]  
        jet_b1 = ak.pad_none(b_jets, 2)[:, 0]
        jet_b2 = ak.pad_none(b_jets, 2)[:, 1]


        light_jets = jets.mask[ak.num(jets) == 2]  
        jet_l1 = ak.pad_none(light_jets, 2)[:, 0]
        jet_l2 = ak.pad_none(light_jets, 2)[:, 1]


        # -------------------------------------------
        # Cross cleaning
        cross_cleaning = (
            (jet_b1.delta_r(jet_b2) > 0.4)
            & (jet_l1.delta_r(jet_l2) > 0.4)
        )

        b1_cc = jet_b1[cross_cleaning]            
        b2_cc = jet_b2[cross_cleaning]      

        l1_cc = jet_l1[cross_cleaning]            
        l2_cc = jet_l2[cross_cleaning]   


        # ---------------------------------------
        # We will define the W using 2j
        # ---------------------------------------

        dijet = l1_cc + l1_cc

        good_w_mass = (
            (dijet.mass > 40) 
            & (dijet.mass < 200) 
        )       

        w = dijet.mask[good_w_mass]        


        # ---------------------------------------
        # We will define the top using the b + 2j
        # ---------------------------------------
        condition_top = (
            ((dijet + b1_cc).mass > 100) 
            & ((dijet + b1_cc).mass < 300)
        )

        trijet = ak.where(
            condition_top,
            l1_cc + l2_cc + b1_cc,
            l1_cc + l2_cc + b2_cc
        )


        good_tops = (
                (trijet.mass > 100)
                & (trijet.mass < 300)
        )      

        top = trijet.mask[good_tops]    



        # ---------------------------------------
        # chi2 criteria
        # ---------------------------------------
        chi2_cal = chi2_test(top, w,  35.02, 24.98, 173.1, 80.4)

        good_chi2 = chi2_cal < 5

        #print(ak.sum(chi2_cal))

        top_scenario_4j = top.mask[good_chi2]
        top_4j_mass = ak.fill_none(top_scenario_4j.mass, 0)
        mask_scenario_4j = ak.fill_none(good_chi2, False)        

        #print("Top tagger_mask_4j (True): ", ak.sum(mask_scenario_4j), mask_scenario_4j.ndim, mask_scenario_4j[mask_scenario_4j == True])

        return top_scenario_4j, mask_scenario_4j, top_4j_mass, ak.sum(mask_scenario_4j)

 