import json
import copy
import pickle
import numpy as np
import awkward as ak
import importlib.resources
from coffea import processor
from coffea.analysis_tools import Weights, PackedSelection
from wprime_plus_b.processors.utils import histograms
from wprime_plus_b.processors.utils.analysis_utils import delta_r_mask, normalize
from wprime_plus_b.corrections.btag import BTagCorrector
from wprime_plus_b.corrections.jec import jet_corrections
from wprime_plus_b.corrections.met import met_phi_corrections
from wprime_plus_b.corrections.pileup import add_pileup_weight
from wprime_plus_b.corrections.pujetid import add_pujetid_weight
from wprime_plus_b.corrections.lepton import ElectronCorrector, MuonCorrector
from wprime_plus_b.selections.qcd.jet_selection import select_good_bjets
from wprime_plus_b.selections.qcd.config import (
    qcd_electron_selection,
    qcd_muon_selection,
    qcd_jet_selection,
)
from wprime_plus_b.selections.qcd.lepton_selection import (
    select_good_electrons,
    select_good_muons,
    select_good_taus,
)


class QcdAnalysis(processor.ProcessorABC):
    """
    QCD Analysis processor

    Parameters:
    -----------
    lepton_flavor:
        lepton flavor {'ele', 'mu'}
    year:
        year of the dataset {"2016", "2017", "2018"}
    year_mode:
        year modifier {"", "APV"}
    btag_wp:
        working point of the deepJet tagger
    syst:
        systematics to apply
    output_type:

    """

    def __init__(
        self,
        lepton_flavor: str = "ele",
        year: str = "2017",
        yearmod: str = "",
        syst: str = "nominal",
        output_type: str = "hist",
    ):
        self._year = year
        self._yearmod = yearmod
        self._lepton_flavor = lepton_flavor
        self._syst = syst
        self._output_type = output_type

        # initialize dictionary of hists for control regions
        self.hist_dict = {
            "met_kin": histograms.qcd_met_hist,
            "lepton_bjet_kin": histograms.qcd_lepton_bjet_hist,
            "lepton_met_kin": histograms.qcd_lepton_met_hist,
            "lepton_met_bjet_kin": histograms.qcd_lepton_met_bjet_hist,
        }
        # define dictionary to store analysis variables
        self.features = {}

    def add_feature(self, name: str, var: ak.Array) -> None:
        """add a variable array to the out dictionary"""
        self.features = {**self.features, name: var}

    def process(self, events):
        # get dataset name
        dataset = events.metadata["dataset"]

        # get number of events before selection
        nevents = len(events)

        # check if sample is MC
        self.is_mc = hasattr(events, "genWeight")

        # create copies of histogram objects
        hist_dict = copy.deepcopy(self.hist_dict)

        for region in ["A", "B", "C", "D"]:
            # ------------------
            # event preselection
            # ------------------

            # ------------------
            # leptons
            # -------------------
            # select good electrons
            good_electrons = select_good_electrons(
                events=events,
                region=region,
            )
            electrons = events.Electron[good_electrons]

            # select good muons
            good_muons = select_good_muons(
                events=events,
                region=region,
            )
            good_muons = (good_muons) & (
                delta_r_mask(events.Muon, electrons, threshold=0.4)
            )
            muons = events.Muon[good_muons]

            # select good taus
            good_taus = (
                select_good_taus(events)
                & (delta_r_mask(events.Tau, electrons, threshold=0.4))
                & (delta_r_mask(events.Tau, muons, threshold=0.4))
            )
            taus = events.Tau[good_taus]

            # ------------------
            # jets
            # -------------------
            # apply JEC/JER corrections to jets (in data, the corrections are already applied)
            if self.is_mc:
                corrected_jets, met = jet_corrections(
                    events, self._year + self._yearmod
                )
            else:
                corrected_jets, met = events.Jet, events.MET

            # select good bjets
            good_bjets = select_good_bjets(
                jets=corrected_jets,
                year=self._year,
                btag_working_point="M",
            )
            good_bjets = (
                good_bjets
                & (delta_r_mask(corrected_jets, electrons, threshold=0.4))
                & (delta_r_mask(corrected_jets, muons, threshold=0.4))
                & (delta_r_mask(corrected_jets, taus, threshold=0.4))
            )
            bjets = corrected_jets[good_bjets]

            # apply MET phi corrections
            met_pt, met_phi = met_phi_corrections(
                met_pt=met.pt,
                met_phi=met.phi,
                npvs=events.PV.npvs,
                is_mc=self.is_mc,
                year=self._year,
                year_mod=self._yearmod,
            )
            met["pt"], met["phi"] = met_pt, met_phi

            # ---------------
            # event selection
            # ---------------
            # make a PackedSelection object to store selection masks
            self.selections = PackedSelection()

            # add luminosity calibration mask (only to data)
            with importlib.resources.path(
                "wprime_plus_b.data", "lumi_masks.pkl"
            ) as path:
                with open(path, "rb") as handle:
                    self._lumi_mask = pickle.load(handle)
            if not self.is_mc:
                lumi_mask = self._lumi_mask[self._year](
                    events.run, events.luminosityBlock
                )
            else:
                lumi_mask = np.ones(len(events), dtype="bool")
            self.selections.add("lumi", lumi_mask)

            # add lepton triggers masks
            with importlib.resources.path(
                "wprime_plus_b.data", "triggers.json"
            ) as path:
                with open(path, "r") as handle:
                    self._triggers = json.load(handle)[self._year]
            trigger = {}
            for ch in ["ele", "mu"]:
                trigger[ch] = np.zeros(nevents, dtype="bool")
                for t in self._triggers[ch]:
                    if t in events.HLT.fields:
                        trigger[ch] = trigger[ch] | events.HLT[t]
            self.selections.add("trigger_ele", trigger["ele"])
            self.selections.add("trigger_mu", trigger["mu"])

            # add MET filters mask
            # open and load met filters
            with importlib.resources.path(
                "wprime_plus_b.data", "metfilters.json"
            ) as path:
                with open(path, "r") as handle:
                    self._metfilters = json.load(handle)[self._year]
            metfilters = np.ones(nevents, dtype="bool")
            metfilterkey = "mc" if self.is_mc else "data"
            for mf in self._metfilters[metfilterkey]:
                if mf in events.Flag.fields:
                    metfilters = metfilters & events.Flag[mf]
            self.selections.add("metfilters", metfilters)

            # cut on MET
            self.selections.add("high_met_pt", met.pt > 50)
            self.selections.add("low_met_pt", met.pt < 50)

            # add number of leptons and jets
            self.selections.add("one_electron", ak.num(electrons) == 1)
            self.selections.add("electron_veto", ak.num(electrons) == 0)
            self.selections.add("one_muon", ak.num(muons) == 1)
            self.selections.add("muon_veto", ak.num(muons) == 0)
            self.selections.add("tau_veto", ak.num(taus) == 0)
            self.selections.add("one_bjet", ak.num(bjets) == 1)

            # define selection regions for each channel
            region_selections = {
                "A": {
                    "ele": [
                        "lumi",
                        "trigger_ele",
                        "metfilters",
                        "high_met_pt",
                        "one_bjet",
                        "tau_veto",
                        "muon_veto",
                        "one_electron",
                    ],
                    "mu": [
                        "lumi",
                        "trigger_mu",
                        "metfilters",
                        "high_met_pt",
                        "one_bjet",
                        "tau_veto",
                        "electron_veto",
                        "one_muon",
                    ],
                },
                "B": {
                    "ele": [
                        "lumi",
                        "trigger_ele",
                        "metfilters",
                        "high_met_pt",
                        "one_bjet",
                        "tau_veto",
                        "muon_veto",
                        "one_electron",
                    ],
                    "mu": [
                        "lumi",
                        "trigger_mu",
                        "metfilters",
                        "high_met_pt",
                        "one_bjet",
                        "tau_veto",
                        "electron_veto",
                        "one_muon",
                    ],
                },
                "C": {
                    "ele": [
                        "lumi",
                        "trigger_ele",
                        "metfilters",
                        "low_met_pt",
                        "one_bjet",
                        "tau_veto",
                        "muon_veto",
                        "one_electron",
                    ],
                    "mu": [
                        "lumi",
                        "trigger_mu",
                        "metfilters",
                        "low_met_pt",
                        "one_bjet",
                        "tau_veto",
                        "electron_veto",
                        "one_muon",
                    ],
                },
                "D": {
                    "ele": [
                        "lumi",
                        "trigger_ele",
                        "metfilters",
                        "low_met_pt",
                        "one_bjet",
                        "tau_veto",
                        "muon_veto",
                        "one_electron",
                    ],
                    "mu": [
                        "lumi",
                        "trigger_mu",
                        "metfilters",
                        "low_met_pt",
                        "one_bjet",
                        "tau_veto",
                        "electron_veto",
                        "one_muon",
                    ],
                },
            }
            # ---------------
            # event variables
            # ---------------
            self.selections.add(
                f"{self._lepton_flavor}_{region}",
                self.selections.all(*region_selections[region][self._lepton_flavor]),
            )
            region_selection = self.selections.all(f"{self._lepton_flavor}_{region}")

            # check that there are events left after selection
            nevents_after = ak.sum(region_selection)
            if nevents_after > 0:
                # select region objects
                region_bjets = bjets[region_selection]
                region_electrons = electrons[region_selection]
                region_muons = muons[region_selection]
                region_met = met[region_selection]

                # define region leptons
                region_leptons = (
                    region_electrons if self._lepton_flavor == "ele" else region_muons
                )
                # lepton relative isolation
                lepton_reliso = (
                    region_leptons.pfRelIso04_all
                    if hasattr(region_leptons, "pfRelIso04_all")
                    else region_leptons.pfRelIso03_all
                )
                # leading bjets
                leading_bjets = ak.firsts(region_bjets)

                # lepton-bjet deltaR and invariant mass
                lepton_bjet_mass = (region_leptons + leading_bjets).mass

                # lepton-MET transverse mass and deltaPhi
                lepton_met_mass = np.sqrt(
                    2.0
                    * region_leptons.pt
                    * region_met.pt
                    * (
                        ak.ones_like(region_met.pt)
                        - np.cos(region_leptons.delta_phi(region_met))
                    )
                )
                # lepton-bJet-MET total transverse mass
                lepton_met_bjet_mass = np.sqrt(
                    (region_leptons.pt + leading_bjets.pt + region_met.pt) ** 2
                    - (region_leptons + leading_bjets + region_met).pt ** 2
                )
                self.add_feature("met", region_met.pt)
                self.add_feature("lepton_bjet_mass", lepton_bjet_mass)
                self.add_feature("lepton_met_mass", lepton_met_mass)
                self.add_feature("lepton_met_bjet_mass", lepton_met_bjet_mass)

                # -------------
                # event weights
                # -------------
                weights_container = Weights(
                    len(events[region_selection]), storeIndividual=True
                )
                if self.is_mc:
                    # add gen weigths
                    gen_weight = events.genWeight[region_selection]
                    weights_container.add("genweight", gen_weight)

                    # add L1prefiring weights
                    if self._year in ("2016", "2017"):
                        weights_container.add(
                            "L1Prefiring",
                            weight=events.L1PreFiringWeight.Nom[region_selection],
                        )
                    # add pileup reweighting
                    add_pileup_weight(
                        n_true_interactions=ak.to_numpy(
                            events.Pileup.nPU[region_selection]
                        ),
                        weights=weights_container,
                        year=self._year,
                        year_mod=self._yearmod,
                        variation="nominal",
                    )
                    # add pujetid weights
                    add_pujetid_weight(
                        jets=region_bjets,
                        weights=weights_container,
                        year=self._year,
                        year_mod=self._yearmod,
                        working_point="T",
                        variation="nominal",
                    )

                    # b-tagging corrector
                    btag_corrector = BTagCorrector(
                        jets=region_bjets,
                        njets=1,
                        weights=weights_container,
                        sf_type="comb",
                        worging_point="M",
                        tagger="deepJet",
                        year=self._year,
                        year_mod=self._yearmod,
                        full_run=False,
                        variation="nominal",
                    )
                    # add b-tagging weights
                    btag_corrector.add_btag_weights(flavor="bc")

                    # electron corrector
                    if self._lepton_flavor == "ele":
                        electron_corrector = ElectronCorrector(
                            electrons=ak.firsts(region_electrons),
                            weights=weights_container,
                            year=self._year,
                            year_mod=self._yearmod,
                            tag="leading_electron",
                            variation="nominal",
                        )
                        # add electron ID weights
                        electron_corrector.add_id_weight(
                            id_working_point="wp90iso"
                            if region in ["A", "B"]
                            else "wp80iso"
                        )
                        # add electron reco weights
                        electron_corrector.add_reco_weight()

                    # muon corrector
                    if self._lepton_flavor == "mu":
                        muon_corrector = MuonCorrector(
                            muons=ak.firsts(region_muons),
                            weights=weights_container,
                            year=self._year,
                            year_mod=self._yearmod,
                            tag="leading_muon",
                            variation="nominal",
                            id_wp="tight" if region in ["A", "B"] else "medium",
                            iso_wp="tight",
                        )
                        # add muon iso weights
                        muon_corrector.add_iso_weight()
                        
                        # how to handle ID and Trigger corrections in the 'medium not tight' case?
                        # add muon ID weights
                        # muon_corrector.add_id_weight()

                        # add muon triggeriso weights
                        # muon_corrector.add_triggeriso_weight()

                # ------------------
                # histogram filling
                # ------------------
                if self._output_type == "hist":
                    for kin in hist_dict:
                        # fill histograms
                        fill_args = {
                            feature: normalize(self.features[feature])
                            for feature in hist_dict[kin].axes.name[:-1]
                            if "dataset" not in feature
                        }
                        hist_dict[kin].fill(
                            **fill_args,
                            dataset=dataset,
                            region=region,
                            weight=weights_container.weight(),
                        )

        # define output dictionary accumulator
        output = {
            "histograms": hist_dict,
            "metadata": {
                "events_before": nevents,
                "events_after": nevents_after,
            },
        }
        # save sumw for MC samples
        if self.is_mc:
            output["metadata"].update({"sumw": ak.sum(events.genWeight)})
        return output

    def postprocess(self, accumulator):
        return accumulator