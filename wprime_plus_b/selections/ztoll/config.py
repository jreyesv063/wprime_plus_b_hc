ztoll_electron_selection = {
         "mu": {
            "electron_pt_threshold": 15,
            "electron_eta_threshold": 2.1,
            "electron_id_wp": "wp90iso",
            "electron_iso_wp": "tight",  
        },
    
         "tau": {
            "electron_pt_threshold": 15,
            "electron_eta_threshold": 2.1,
            "electron_id_wp": "wp90iso",
            "electron_iso_wp": "tight",  
        },
}

ztoll_muon_selection = {
        "mu": {
            "muon_pt_threshold": 28, 
            "muon_eta_threshold": 2.1,
            "muon_id_wp": "tight",
            "muon_iso_wp": "tight",           
        },
        "tau": {
            "muon_pt_threshold": 15, 
            "muon_eta_threshold": 2.1,
            "muon_id_wp": "tight",
            "muon_iso_wp": "tight",           
        },
}


ztoll_jet_selection = {
         "mu": {
            "jet_pt_threshold": 20,
            "jet_eta_threshold": 2.4,
            "btag_working_point": "M",
            "jet_id": 6,
            "jet_pileup_id": 7        
        },
         "tau": {
            "jet_pt_threshold": 20,
            "jet_eta_threshold": 2.4,
            "btag_working_point": "M",
            "jet_id": 6,
            "jet_pileup_id": 7        
        },
}

ztoll_tau_selection = {
         "mu": {
            "tau_pt_threshold": 20,  
            "tau_eta_threshold": 2.1, 
            "tau_dz_threshold": 0.2, 
            "tau_Jet": "Tight",
            "tau_e": "Tight", 
            "tau_mu": "Tight",
            "prongs": 13,
        },
         "tau": {
            "tau_pt_threshold": 70,  
            "tau_eta_threshold": 2.1, 
            "tau_dz_threshold": 0.2, 
            "tau_Jet": "Tight",
            "tau_e": "Tight", 
            "tau_mu": "Tight",
            "prongs": 13,
        },
}

ztoll_cross_cleaning_selection = {
         "mu": {
            "DR": 0.4,  
        },
         "tau": {
            "DR": 0.5,  
        },
}

ztoll_met_selection = {
        "mu": {
            "met_threshold": 0,
        },
        "tau": {
            "met_threshold": 30,
        },
}

ztoll_dilepton_selection = {
        "mu": {
            "OS": True,
            "LS": False ,
            "mll": 100,
        },  
        "tau": {
            "OS": True,
            "LS": False ,
            "mll": 100,
        },   
}
    
ztoll_trigger_selection = {
    "mu": {
            "trigger": "mu"
    },
    "tau": {
            "trigger": "tau_Zprime_thesis"
    },
}
