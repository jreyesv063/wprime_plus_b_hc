ttbar_electron_selection = {
     "2b1tau": {
         "tau": {
            "electron_pt_threshold": 30,
            "electron_eta_threshold": 2.4,
            "electron_id_wp": "medium",
            "electron_iso_wp": "tight",  
        },
     },
}

ttbar_muon_selection = {
     "2b1tau": {
        "tau": {
            "muon_pt_threshold": 25, 
            "muon_eta_threshold": 2.1,
            "muon_id_wp": "tight",
            "muon_iso_wp": "tight",           
        },
     },
}


ttbar_jet_selection = {
     "2b1tau": {
         "tau": {
            "jet_pt_threshold": 20,
            "jet_eta_threshold": 2.4,
            "btag_working_point": "T",
            "jet_id": 6,
            "jet_pileup_id": 7        
        },
     },
}

ttbar_tau_selection = {
     "2b1tau": {
         "tau": {
            "tau_pt_threshold": 20,  
            "tau_eta_threshold": 2.3, 
            "tau_dz_threshold": 0.2, 
            "tau_Jet": "Tight",
            "tau_e": "Tight", 
            "tau_mu": "Tight",
            "prongs": 13,
        },
     },
}

ttbar_cross_cleaning_selection = {
     "2b1tau": {
         "tau": {
            "DR": 0.4,  
        },
     },   
}

ttbar_met_selection = {
     "2b1tau": {
        "tau": {
            "met_threshold": 250,
            "deltaPhi_met_tau": 2.4,
        },
     }   
}

ttbar_trigger_selection = {
     "2b1tau": {
        "tau": {
            "trigger": "tau"
        },
     },
    
}
