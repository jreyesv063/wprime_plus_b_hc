import correctionlib
import numpy as np
import awkward as ak
from typing import Tuple
from wprime_plus_b.corrections.utils import get_pog_json


def met_phi_corrections(
    met_pt: ak.Array,
    met_phi: ak.Array,
    npvs: ak.Array,
    run: ak.Array,
    is_mc: bool,
    year: str,
    year_mod: str = "",
) -> Tuple[ak.Array, ak.Array]:
    """
    Apply MET phi modulation corrections

    Parameters:
    -----------
        met_pt:
            MET transverse momentum
        met_phi:
            MET azimuthal angle
        npvs:
            Total number of reconstructed primary vertices
        is_mc:
            True if dataset is MC
        year:
            Year of the dataset {'2016', '2017', '2018'}
        year_mod:
            Year modifier {'', 'APV'}

    Returns:
    --------
        corrected MET pt and phi
        https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETRun2Corrections#xy_Shift_Correction_MET_phi_modu
    """
    cset = correctionlib.CorrectionSet.from_file(
        get_pog_json(json_name="met", year=year + year_mod)
    )
    
    met_pt_orig = met_pt  # Llena con los valores originales
    met_phi_orig = met_phi  # Llena con los valores originales
    
    # https://gitlab.cern.ch/cms-tau-pog/jsonpog-integration/-/blob/master/POG/JME/2017_UL/met.json.gz?ref_type=heads
    met_mask = (met_pt < 6500)  # See the json file
    
    met_pt_limit = met_pt.mask[met_mask]
    met_phi_limit = met_phi.mask[met_mask]
    good_npvs_limit = npvs.mask[met_mask] 
    run_number_limit = run.mask[met_mask]
    
    pt = ak.fill_none(met_pt_limit, 0.0)
    phi = ak.fill_none(met_phi_limit, 0.0)
    good_npvs = ak.fill_none(good_npvs_limit, 0.0)
    run_number = ak.fill_none(run_number_limit,  1)
    
    if is_mc:
        # MC corrections
        corrected_met_pt = cset[f"pt_metphicorr_pfmet_mc"].evaluate(pt, phi, good_npvs, run_number)
        corrected_met_phi = cset[f"phi_metphicorr_pfmet_mc"].evaluate(pt, phi, good_npvs, run_number)
        
        
    else:
        # Data corrections
        corrected_met_pt = cset[f"pt_metphicorr_pfmet_data"].evaluate(pt, phi, good_npvs, run_number)
        corrected_met_phi = cset[f"phi_metphicorr_pfmet_data"].evaluate(pt, phi, good_npvs, run_number)
        
    

    # Reconstruir met_pt y met_phi
    corrected_met_pt = np.where(met_mask, corrected_met_pt, met_pt_orig)
    corrected_met_phi = np.where(met_mask, corrected_met_phi, met_phi_orig)

    return corrected_met_pt, corrected_met_phi    
    
  
