import correctionlib
import copy
import numpy as np
import awkward as ak
import importlib.resources
from typing import Type
from coffea.analysis_tools import Weights
from wprime_plus_b.corrections.utils import pog_years, get_pog_json  
from coffea.nanoevents.methods import vector # TLorentz vector

def mask_energy_corrections(tau):
    tau_mask_gm = (
        (tau.genPartFlav == 1)
        | (tau.genPartFlav == 2)
        | (tau.genPartFlav == 5)
        | (tau.genPartFlav == 6)
    )
    
    tau_mask_dm = (
        (tau.decayMode == 0) |
        (tau.decayMode == 1) |
        (tau.decayMode == 2) |
        (tau.decayMode == 10) |
        (tau.decayMode == 11)
    )
    
    tau_eta_mask = (
        (tau.eta >= 0)
        & (tau.eta < 2.5)
    )
    
    tau_mask = (tau_mask_gm & tau_eta_mask & tau_mask_dm)
    
    return tau_mask
    

def energy_scale(
    taus: ak.Array,
    year: str = "2017",
    year_mod: str = "",
    id: str = "DeepTau2017v2p1",
    sys: str = "nom"
    ):
    
    
    # Corrections works with flatten values 
    ntaus = ak.num(taus)
    taus_flatten = ak.flatten(taus)

    
    # It is defined the taus will be corrected with the energy scale factor: Only a subset of the initial taus.
    mask = mask_energy_corrections(taus_flatten)
    
    taus_filter = taus_flatten[mask]
    
    
    # Define correction set_id
    cset = correctionlib.CorrectionSet.from_file(
        get_pog_json(json_name="tau", year=year + year_mod)
    )
    
    SF = cset["tau_energy_scale"].evaluate(taus_filter.pt, 
                                      taus_filter.eta,
                                      taus_filter.decayMode,
                                      taus_filter.genPartFlav,
                                      id,
                                      sys
                                     )
     
    # Calculate the new pt and mass
    taus_new_pt = taus_filter.pt * SF
    taus_new_mass = taus_filter.mass * SF
    
    
    taus_new = ak.zip(
        {"pt": taus_new_pt, 
         "mass": taus_new_mass,
         **{name: taus_filter[name] for name in taus_filter.fields if name not in ["pt", "mass"]}}
    )
                  

    # Update our taus fixed and not modified the taus didn't fix.
    taus_flatten_list = ak.to_list(taus_flatten)

    # Iterate over the mask and update only the positions where the mask is True.
    count_correction = 0
    for idx, should_update in enumerate(mask):
        if should_update == True:
            taus_flatten_list[idx] = taus_new[count_correction]
            #print(f"{should_update =} and {count_correction}: Scale factor {SF[count_correction]}")
            count_correction += 1
            
    taus_flatten_updated = ak.Array(taus_flatten_list)
    
    taus_unflatten = ak.unflatten(taus_flatten_updated , ntaus)
    
    
    
    return taus_unflatten#, mask, SF



def energy_scale_mask(
    taus: ak.Array,
    year: str = "2017",
    year_mod: str = "",
    id: str = "DeepTau2017v2p1",
    sys: str = "nom"
    ):

    # Corrections works with flatten values 
    ntaus = ak.num(copy.deepcopy(taus))
    taus_flatten = ak.flatten(copy.deepcopy(taus))

    # It is defined the taus will be corrected with the energy scale factor: Only a subset of the initial taus.
    mask = mask_energy_corrections(taus_flatten)
    
    taus_filter = taus_flatten[mask]
    

    # Define correction set_id
    cset = correctionlib.CorrectionSet.from_file(
        get_pog_json(json_name="tau", year=year + year_mod)
    )

    SF = cset["tau_energy_scale"].evaluate(taus_filter.pt, 
                                           taus_filter.eta,
                                           taus_filter.decayMode,
                                           taus_filter.genPartFlav,
                                           id,
                                           sys
                                          )

    taus_new_pt = ak.to_numpy(taus_filter.pt * SF)
    taus_new_mass = ak.to_numpy(taus_filter.mass * SF)
    
    taus_flatten_pt = ak.to_numpy(taus_flatten.pt)
    taus_flatten_mass = ak.to_numpy(taus_flatten.mass)
    
    
    taus_flatten_pt[mask] = taus_new_pt[:np.count_nonzero(mask)]
    taus_flatten_mass[mask] = taus_new_mass[:np.count_nonzero(mask)]
    
    taus_pt_corrected = ak.from_numpy(taus_flatten_pt)
    taus_mass_corrected = ak.from_numpy(taus_flatten_mass)
    

    taus_corrected = ak.zip(
        {"pt": taus_pt_corrected, **{name: taus_flatten[name] for name in taus_flatten.fields if name != "pt"}}
    )
    
    
    taus_corrected = ak.zip(
        {"pt":taus_pt_corrected,
         "mass": taus_mass_corrected,
         ** {name: taus_flatten[name] for name in taus_flatten.fields if name not in ["pt", "mass"]}
        }
    )
    
    taus_unflatten = ak.unflatten(taus_corrected , ntaus)
    
    
    # https://coffeateam.github.io/coffea/modules/coffea.nanoevents.methods.vector.html
    # In order not to cause conflicts in the code, we must preserve that taus_unflatten is a TLorentz.
    taus_unflatten_TLorentz = ak.zip(
        {
            "pt": taus_unflatten.pt,
            "eta": taus_unflatten.eta,
            "phi": taus_unflatten.phi,
            "mass": taus_unflatten.mass,
            "dz": taus_unflatten.dz,
           # "idDecayModeNewDMs": taus_unflatten.idDecayModeNewDMs, # Commente en 8/01/2024
            "idDeepTau2017v2p1VSjet": taus_unflatten.idDeepTau2017v2p1VSjet,  
            "idDeepTau2017v2p1VSe":  taus_unflatten.idDeepTau2017v2p1VSe,  
            "idDeepTau2017v2p1VSmu": taus_unflatten.idDeepTau2017v2p1VSmu,
            "decayMode": taus_unflatten.decayMode,
            "genPartFlav": taus_unflatten.genPartFlav, 
            
            
            "chargedIso": taus_unflatten.chargedIso,
            "dxy": taus_unflatten.dxy,
            "leadTkDeltaEta": taus_unflatten.leadTkDeltaEta,
            "leadTkDeltaPhi": taus_unflatten.leadTkDeltaPhi,
            "leadTkPtOverTauPt": taus_unflatten.leadTkPtOverTauPt,
            "neutralIso": taus_unflatten.neutralIso,
            "photonsOutsideSignalCone": taus_unflatten.photonsOutsideSignalCone,
            "puCorr": taus_unflatten.puCorr,
            #"rawAntiEle": taus_unflatten.rawAntiEle, # Commente en 8/01/2024
            #"rawAntiEle2018": taus_unflatten.rawAntiEle2018, # Commente en 8/01/2024
            "rawDeepTau2017v2p1VSe": taus_unflatten.rawDeepTau2017v2p1VSe,
            "rawDeepTau2017v2p1VSjet": taus_unflatten.rawDeepTau2017v2p1VSjet,
            "rawDeepTau2017v2p1VSmu": taus_unflatten.rawDeepTau2017v2p1VSmu,
            "rawIso": taus_unflatten.rawIso,
            "rawIsodR03": taus_unflatten.rawIsodR03,
           # "rawMVAnewDM2017v2": taus_unflatten.rawMVAnewDM2017v2,   # Commente en 8/01/2024
           # "rawMVAoldDM": taus_unflatten.rawMVAoldDM,  # Commente en 8/01/2024
           # "rawMVAoldDM2017v1": taus_unflatten.rawMVAoldDM2017v1,  # Commente en 8/01/2024
           # "rawMVAoldDM2017v2": taus_unflatten.rawMVAoldDM2017v2,  # Commente en 8/01/2024
           # "rawMVAoldDMdR032017v2": taus_unflatten.rawMVAoldDMdR032017v2,  # Commente en 8/01/2024
            "charge": taus_unflatten.charge,
            "jetIdx": taus_unflatten.jetIdx,
           # "rawAntiEleCat": taus_unflatten.rawAntiEleCat,  # Commente en 8/01/2024
           # "rawAntiEleCat2018": taus_unflatten.rawAntiEleCat2018,  # Commente en 8/01/2024
           # "idAntiEle": taus_unflatten.idAntiEle,    # Commente en 8/01/2024
           # "idAntiEle2018": taus_unflatten.idAntiEle2018,  # Commente en 8/01/2024
            "idAntiEleDeadECal": taus_unflatten.idAntiEleDeadECal,
            "idAntiMu": taus_unflatten.idAntiMu,
           # "idDecayMode":  taus_unflatten.idDecayMode,  # Commente en 8/01/2024
           # "idMVAnewDM2017v2": taus_unflatten.idMVAnewDM2017v2,  # Commente en 8/01/2024
           # "idMVAoldDM": taus_unflatten.idMVAoldDM, # Commente en 8/01/2024
           # "idMVAoldDM2017v1": taus_unflatten.idMVAoldDM2017v1, # Commente en 8/01/2024
           # "idMVAoldDM2017v2": taus_unflatten.idMVAoldDM2017v2, # Commente en 8/01/2024
           # "idMVAoldDMdR032017v2": taus_unflatten.idMVAoldDMdR032017v2, # Commente en 8/01/2024
            "cleanmask": taus_unflatten.cleanmask,
            "genPartIdx": taus_unflatten.genPartIdx,
            "genPartIdxG": taus_unflatten.genPartIdxG,
            "jetIdxG": taus_unflatten.jetIdxG
        },
    with_name="PtEtaPhiMLorentzVector",
    behavior=vector.behavior,
    )

    
    return taus_unflatten_TLorentz 






