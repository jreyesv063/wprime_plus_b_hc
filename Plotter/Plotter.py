import json
import awkward as ak
import numpy as np
import pandas as pd
from utils import open_output, open_metadata, group_outputs, get_hist,  plot_hist, get_cuts, list_plots, generate_report
from coffea import processor


class Plotter:
    
    def __init__(
        self,
        year: str = "2017",
        year_mod: str = "",
        output: str = "../outfiles",
        lepton: str = "mu",
    ) -> None:
        
        # year
        self.year = year
        self.year_mod = year_mod 
        
        self.output_directory = output 
        
        self.lepton = lepton
        
        
        # load cross sections
        with open(f"../wprime_plus_b/data/DAS_xsec.json", "r") as f:
            self.xsecs = json.load(f)
            
        # load luminosity
        with open(f"../wprime_plus_b/data/luminosity.json", "r") as f:
            luminosity = json.load(f)
            
        # define luminosity
        if year == "2016":
            self.lumi = luminosity[year][year_mod]

            
        else:
            self.lumi = luminosity[year]

        
    def lumi_xsecs(self):
        return self.lumi, self.xsecs


    # Calculation of weights
    
    def get_weights(self,first_accumulated_metadata):
        
        weights = {}
        
        for sample in first_accumulated_metadata:
            if sample not in ["SingleElectron", "SingleMuon", "SingleTau", "MET"]:
                weights[sample] = self.lumi * self.xsecs[sample] / first_accumulated_metadata[sample]["sumw_before"]
            else:
                weights[sample] = 1        
        
        return weights
    

    def get_raw_events(self):
                
        grouped_outputs_metadata = group_outputs(self.output_directory, metadata=True)

        # get the raw number of events before and after selection from metadata
        grouped_metadata = {}
        first_accumulated_metadata = {}
        for sample in grouped_outputs_metadata:
            grouped_metadata[sample] = []
            for fname in grouped_outputs_metadata[sample]:
                output = open_metadata(fname)
                meta = {}
                meta["sumw_before"] = output["sumw"]
                meta["events_before"] = output["events_before"]
                meta["events_after"] = output["events_after"]
                grouped_metadata[sample].append(meta)
            first_accumulated_metadata[sample] = processor.accumulate(grouped_metadata[sample]) 
            
        return first_accumulated_metadata
    # Table report: The following method return the table with the data and Background contribution.
    
    def get_table_report(self):
        
        # Obtain weights
        raw_metadata = self.get_raw_events()
        weights = self.get_weights(raw_metadata)
        
        # group outputs files by sample name
        grouped_outputs = group_outputs(self.output_directory)

        # group output arrays by sample name
        grouped_histograms = {}
        first_accumulated_histograms = {}
        for sample in grouped_outputs:    
            grouped_histograms[sample] = []
            for fname in grouped_outputs[sample]:
                output = open_output(fname)["arrays"]
                if len(output) == 0: continue
                if sample not in ["SingleElectron", "SingleMuon", "SingleTau", "MET"]:
                    grouped_histograms[sample].append({"w": output["weights"]})
                else:
                    grouped_histograms[sample].append({"w": output["weights"]})
            first_accumulated_histograms[sample] = processor.accumulate(grouped_histograms[sample])

        # create report
        mcs = ["DYJetsToLL", "WJetsToLNu", "VV", "VVV", "tt", "SingleTop", "Higgs"]
        events = {sample: 0 for sample in mcs}
        events.update({"Data": 0})
        errors = events.copy()

        for sample in raw_metadata:
            if ("SingleElectron" in sample) or ("SingleMuon" in sample) or ("SingleTau" in sample) or ("MET" in sample):
                events["Data"] += raw_metadata[sample]["events_after"]
                errors["Data"] += np.sqrt(events["Data"])
                continue

            if first_accumulated_histograms[sample] is None:
                continue
            # get number of events from weights vector (in order to include scale factors)
            nevents = ak.sum(first_accumulated_histograms[sample]["w"].value)

            # scale to lumi-xsec
            nevents *= weights[sample] 

            # get statistical error (using raw MC events)
            n_mc = raw_metadata[sample]["events_after"]
            error = (self.lumi * self.xsecs[sample] / raw_metadata[sample]["events_before"]) * np.sqrt(n_mc)

            if "DYJetsToLL" in sample:
                events["DYJetsToLL"] += nevents
                errors["DYJetsToLL"] += error
            elif "WJetsToLNu" in sample:
                events["WJetsToLNu"] += nevents
                errors["WJetsToLNu"] += error
            elif (sample == "WW") or (sample == "WZ") or (sample == "ZZ"):
                events["VV"] += nevents
                errors["VV"] += error
            elif "TTT" in sample:
                events["tt"] += nevents
                errors["tt"] += error
            elif "ST" in sample:
                events["SingleTop"] += nevents
                errors["SingleTop"] += error
            elif ("VBFH" in sample) or ("GluGluH" in sample):
                events["Higgs"] += nevents
                errors["Higgs"] += error

        # add number of expected events and errors to report
        report_df = pd.DataFrame(columns=["events", "error", "percentage"])
        for sample in events:
            report_df.loc[sample, "events"] = events[sample]
            report_df.loc[sample, "error"] = errors[sample]

        # add percentages to report
        mcs_output = report_df.loc[mcs].copy()
        report_df.loc[mcs, "percentage"] = (
            mcs_output["events"] / mcs_output["events"].sum()
        ) * 100

        # (https://depts.washington.edu/imreslab/2011%20Lectures/ErrorProp-CountingStat_LRM_04Oct2011.pdf)
        # add total background number of expected events and error to report
        report_df.loc["Total bkg", "events"] = np.sum(report_df.loc[mcs, "events"])
        report_df.loc["Total bkg", "error"] = np.sqrt(
            np.sum(report_df.loc[mcs, "error"] ** 2)
        )
        
        # add data to bacground ratio and error
        data = report_df.loc["Data", "events"]
        data_err = report_df.loc["Data", "error"]
        bkg = report_df.loc["Total bkg", "events"]
        bkg_err = report_df.loc["Total bkg", "error"]

        report_df.loc["Data/bkg", "events"] = data / bkg
        report_df.loc["Data/bkg", "error"] = (data / bkg) * np.sqrt(
            (bkg_err/bkg) ** 2 +  (data_err/data)**2 
        )
        
        # sort processes by percentage
        report_df = report_df.loc[mcs + ["Total bkg", "Data", "Data/bkg"]]
        report_df = report_df.sort_values(by="percentage", ascending=False)

        # drop process with no events
        report_df = report_df.loc[report_df.sum(axis=1) > 0]

        return report_df
    
    
        
        
    def get_plot_report(self, distribution: str, list_histo: bool, divided_GeV: bool):        
            
        if list_histo:
            
            list_h = list_plots()
            
            
             # Generamos el informe utilizando el diccionario de histogramas
            report = generate_report(list_h)
            

            # Imprimimos el informe
            print(report)
            
            return
        
        
        # Obtain weights
        raw_metadata = self.get_raw_events()
        weights = self.get_weights(raw_metadata)
        

        my_hist = get_hist(distribution, self.output_directory, weights)
        plot_hist(my_hist, distribution, lepton_flavor=self.lepton, Events_GeV = divided_GeV, yratio_limits=(0, 2))
            

            
        
            
    
    def get_table_cutflow(self):
                
        grouped_outputs_metadata = group_outputs(self.output_directory, metadata=True)
        
        grouped_metadata = {}
        first_accumulated_metadata = {}
        
        for sample in grouped_outputs_metadata:
            grouped_metadata[sample] = []
            for fname in grouped_outputs_metadata[sample]:
                output = open_metadata(fname)
                cutflow = {}
                
                # Initial number of events
                sumw = float(output["sumw"])
                cutflow["sumw"] = sumw
                for cutname in output["cutflow"]:
                    cutflow[cutname] = float(output["cutflow"][cutname])
                grouped_metadata[sample].append(cutflow)
            first_accumulated_metadata[sample] = processor.accumulate(grouped_metadata[sample])


        # Obtain weights
        raw_metadata = self.get_raw_events()
        weights = self.get_weights(raw_metadata)
        
        for sample in first_accumulated_metadata:
            for cut in first_accumulated_metadata[sample]:
                first_accumulated_metadata[sample][cut] = weights[sample] * first_accumulated_metadata[sample][cut]
            
        samples_list, cuts_list = get_cuts(first_accumulated_metadata)
        
        # Initialize the cumulative numbers
        
        n_tt = {cut: 0 for cut in cuts_list}  # cuts_list debe ser una lista de los cortes que estás usando
        n_st = {cut: 0 for cut in cuts_list}
        n_dy = {cut: 0 for cut in cuts_list}
        n_wj = {cut: 0 for cut in cuts_list}
        n_vv = {cut: 0 for cut in cuts_list}
        n_data = {cut: 0 for cut in cuts_list}
        n_back = {cut: 0 for cut in cuts_list}
        n_ratio = {cut: 0 for cut in cuts_list}

        n_events = 0.0
        
        
        for sample in first_accumulated_metadata:
            for cut in first_accumulated_metadata[sample]:
                if sample.startswith('TTTo'):
                    n_events = first_accumulated_metadata[sample][cut]
                    n_tt[cut] +=  n_events
                    n_back[cut] += n_events
                    
                elif sample.startswith('ST'):
                    n_events = first_accumulated_metadata[sample][cut] 
                    n_st[cut] += n_events 
                    n_back[cut] +=  n_events 
                    
                elif sample.startswith('DYJetsToLL'):
                    n_events = first_accumulated_metadata[sample][cut] 
                    n_dy[cut] += n_events
                    n_back[cut] +=  n_events 
                    
                elif sample.startswith('WJetsToLNu'):
                    n_events = first_accumulated_metadata[sample][cut] 
                    n_wj[cut] += n_events
                    n_back[cut] +=  n_events 
                    
                elif sample == "WW" or sample == "WZ" or sample == "ZZ":
                    n_events = first_accumulated_metadata[sample][cut]
                    n_vv[cut] += n_events
                    n_back[cut] +=  n_events 
                    
                elif sample.startswith('Single') or sample.startswith('MET'):
                    n_data[cut] += first_accumulated_metadata[sample][cut] 
                if n_back[cut] != 0:
                    n_ratio[cut] = n_data[cut]/n_back[cut]
                
            
        #print(f"{n_tt=}; {n_st=}; {n_dy=}; {n_wj=}; {n_vv=}; {n_data=} ")
  
        # Create report
        data = {
            'Data': {key: round(value, 1) for key, value in n_data.items()},
            'tt': {key: round(value, 1) for key, value in n_tt.items()},
            'SingleTop': {key: round(value, 1) for key, value in n_st.items()},
            'DYJetsToLL': {key: round(value, 1) for key, value in n_dy.items()},
            'WJetsToLNu': {key: round(value, 1) for key, value in n_wj.items()},
            'VV': {key: round(value, 1) for key, value in n_vv.items()},
            'Total_Back': {key: round(value, 1) for key, value in n_back.items()},
            "Data/Total_Back": {key: round(value, 1) for key, value in n_ratio.items()}
        }
        
            
        # Create the dataframe using pandas
        df = pd.DataFrame(data)
            
        return df

