import pickle
import json
import glob
import hist
import copy
import numpy as np
import pandas as pd
from coffea import processor
import mplhep as hep
import matplotlib.pyplot as plt
from intervals import poisson_interval, ratio_uncertainty

def open_output(fname):
    with open(fname, "rb") as f:
        h = pickle.load(f)
    return h

def open_metadata(fname):
    with open(fname, "r") as f:
        metadata = json.load(f)
    return metadata

def group_outputs(output_directory: str, metadata=False) -> dict:
    """group output .pkl files by sample"""
    if metadata:
        extension = ".json"
        output_files = glob.glob(f"{output_directory}/metadata/*{extension}", recursive=True)
    else:
        extension = ".pkl"
        output_files = glob.glob(f"{output_directory}/*{extension}", recursive=True)
        
    grouped_outputs = {}
    for output_file in output_files:
        # get output file names
        sample_name = output_file.replace("_metadata", "").split("/")[-1].split(extension)[0]
        if sample_name.rsplit("_")[-1].isdigit():
            sample_name = "_".join(sample_name.rsplit("_")[:-1])
        # append file names to grouped_outputs
        if sample_name in grouped_outputs:
            grouped_outputs[sample_name].append(output_file)
        else:
            grouped_outputs[sample_name] = [output_file]
    return grouped_outputs

def get_label_map(channel: str):
    label_map = {
    "ele": {
        "jet_pt": r"$p_T$(b-Jet$_{0}$) [GeV]",
        "jet_eta": r"$\eta$(b-Jet$_{0}$)",
        "jet_phi": r"$\phi$(b-Jet$_{0}$)",
        "met": r"$p_T^{miss}$ [GeV]",
        "met_phi": r"$\phi(p_T^{miss})$",
        "lepton_pt": r"$p_T(e)$ [GeV]",
        "lepton_relIso": "$e$ RelIso",
        "lepton_eta": r"$\eta(e)$",
        "lepton_phi": r"$\phi (e)$",
        "lepton_bjet_mass": r"$m(e, $b-Jet$_{0})$ [GeV]",
        "lepton_bjet_dr": r"$\Delta R$($e$, b-Jet$_{0}$)",
        "lepton_met_mass": r"$m_T$($e$, $p_T^{miss}$) [GeV]",
        "lepton_met_delta_phi": r"$\Delta \phi(e, p_T^{miss})$",
        "lepton_met_bjet_mass": r"$m_T^{tot}(e, $b-Jet$_{0}, p_T^{miss})$ [GeV]",
        "dilepton_mass": r"$m_{ee}$ [GeV]"
    },
    "mu": {
        "jet_pt": r"$p_T$(b-Jet$_{0}$) [GeV]",
        "jet_eta": r"$\eta$(b-Jet$_{0}$)",
        "jet_phi": r"$\phi$(b-Jet$_{0}$)",
        "met": r"$p_T^{miss}$ [GeV]",
        "met_phi": r"$\phi(p_T^{miss})$",
        "lepton_pt": r"$p_T(\mu)$ [GeV]",
        "lepton_relIso": "$\mu$ RelIso",
        "lepton_eta": r"$\eta(\mu)$",
        "lepton_phi": r"$\phi (\mu)$",
        "lepton_bjet_mass": r"$m(\mu, $b-Jet$_{0})$ [GeV]",
        "lepton_bjet_dr": r"$\Delta R$($\mu$, b-Jet$_{0}$)",
        "lepton_met_mass": r"$m_T$($\mu$, $p_T^{miss}$) [GeV]",
        "lepton_met_delta_phi": r"$\Delta \phi(\mu, p_T^{miss})$",
        "lepton_met_bjet_mass": r"$m_T^{tot}(\mu, $b-Jet$_{0}, p_T^{miss})$ [GeV]",
        "dilepton_mass": r"$m_{\mu \mu}$ [GeV]"
    },
    "tau": {
        "jet_pt": r"$p_T$(b-Jet$_{0}$) [GeV]",
        "jet_eta": r"$\eta$(b-Jet$_{0}$)",
        "jet_phi": r"$\phi$(b-Jet$_{0}$)",
        "number_bjets": r"N(b-Jet)",
        "met": r"$p_T^{miss}$ [GeV]",
        "met_phi": r"$\phi(p_T^{miss})$",
        "lepton_pt": r"$p_T(\tau)$ [GeV]",
        "lepton_relIso": "$\tau$ RelIso",
        "lepton_eta": r"$\eta(\tau)$",
        "lepton_phi": r"$\phi (\tau)$",
        "lepton_bjet_mass": r"$m(\tau, $b-Jet$_{0})$ [GeV]",
        "lepton_bjet_dr": r"$\Delta R$($\tau$, b-Jet$_{0}$)",
        "lepton_met_mass": r"$m_T$($\tau$, $p_T^{miss}$) [GeV]",
        "lepton_met_delta_phi": r"$\Delta \phi(\tau, p_T^{miss})$",
        "lepton_met_bjet_mass": r"$m_T^{tot}(\tau, $b-Jet$_{0}, p_T^{miss})$ [GeV]",
        "dilepton_mass": r"$m_{\tau \tau}$ [GeV]",     
        
        "top_mrec": r"$m_{rec}(top)$ [GeV]",
        "w_mrec": r"$m_{rec}(W)$ [GeV]",
        
        "Z_mrec": r"$m_{rec}(Z)$ [GeV]",
        "Z_charge": r"$Q(Z)$",
        "lepton_one_pt": r"$p_{T}(\tau_{1})$ [GeV]",
        "lepton_two_pt": r"$p_{T}(\tau_{2})$ [GeV]",
        "l1_charge":  r"$Q(\tau_{1})$",
        "l2_charge": r"$Q(\tau_{1})$",
        "met_ztoll": r"$p_{T}^{miss}$",
    }
    }
    
    return label_map[channel]

def get_sample_map(sample: str):
    
    sample_map = {
        "VV": "Diboson",
        "SingleTop": "Single Top",
        "WJetsToLNu": r"W$(\ell \nu)$+jets",
        "tt": r"$t\bar{t}$",
        "DYJetsToLL": r"DY$(\ell\ell)$+jets",
        "Higgs": "Higgs",
        "SingleMuon": "Data",
        "SingleElectron": "Data",
        "SingleTau": "Data",
        "MET": "Data",
    }
    
    return sample_map[sample]


def get_hist_kwargs():
    mc_hist_kwargs = {
        "histtype": "fill",
        "stack": True,
        "sort": "yield",
        "linewidth": 0.7,
        "edgecolor": "k",
    }
    data_hist_kwargs = {
        "histtype": "errorbar",
        "color": "k",
        "linestyle": "none",
        "marker": ".",
        "markersize": 10.0,
        "elinewidth": 1,
        "yerr": True,
        "xerr": True,
        "linestyle": "none",
        "marker": ".",
    }
    
    errors_hist_kwargs = {
        "color": "lightgray",
        "hatch": "////",
        "alpha": 0.6,
        "edgecolor": "k",
        "linewidth": 0,
    }
    
    return mc_hist_kwargs, data_hist_kwargs, errors_hist_kwargs


def group_histograms(scaled_histograms: dict) -> dict:
    """group scaled histograms by process"""
    hists = {
        "DYJetsToLL": [],
        "WJetsToLNu": [],
        "VV": [],
        "tt": [],
        "SingleTop": [],
        "Data": []
    }
    for sample in scaled_histograms:
        if "DYJetsToLL" in sample:
            hists["DYJetsToLL"].append(scaled_histograms[sample])
        elif "WJetsToLNu" in sample:
            hists["WJetsToLNu"].append(scaled_histograms[sample])
        elif (sample == "WW") or (sample == "WZ") or (sample == "ZZ"):
            hists["VV"].append(scaled_histograms[sample])
        elif "TTT" in sample:
            hists["tt"].append(scaled_histograms[sample])
        elif "ST" in sample:
            hists["SingleTop"].append(scaled_histograms[sample])
        elif sample in ["SingleMuon", "SingleElectron", "SingleTau", "MET"]:
            hists["Data"] = scaled_histograms[sample]

    
    for sample in hists:
        if sample == "Data": continue
        hists[sample] = processor.accumulate(hists[sample])
        
    return hists



def get_hist(feature, output_directory, weights):
    
    # group outputs files by sample name
    grouped_outputs = group_outputs(output_directory)

    # group output arrays by sample name
    grouped_histograms = {}
    first_accumulated_histograms = {}
    for sample in grouped_outputs:
        grouped_histograms[sample] = []
        for fname in grouped_outputs[sample]:
            output = open_output(fname)["arrays"]
            if len(output) == 0:
                continue
            features = {feature: output[feature]}
            grouped_histograms[sample].append(features)
            features.update({"weights": output["weights"]})
        first_accumulated_histograms[sample] = processor.accumulate(grouped_histograms[sample])
    
    # fill histograms and scale them to lumi-xsec
    
    # --------------------------
    # histogram axes definition
    # --------------------------
    # systematics axis
    syst_axis = hist.axis.StrCategory([], name="variation", growth=True)

    # dataset axis
    dataset_axis = hist.axis.StrCategory([], name="dataset", growth=True)

    
    # jet axes
    jet_pt_axis = hist.axis.Variable(
        edges= [20,60,100,140,180,220, 300, 350, 400, 500],#[20, 60, 90, 120, 150, 180, 210, 240, 300, 500],
        name="jet_pt",
    )
    jet_pt_hist = hist.Hist(
        jet_pt_axis,
        hist.storage.Weight(),
    )
    
    jet_eta_axis = hist.axis.Regular(
        bins=50,
        start=-2.4,
        stop=2.4,
        name="jet_eta",
    )
    jet_eta_hist = hist.Hist(
        jet_eta_axis,
        hist.storage.Weight(),
    )
    
    jet_phi_axis = hist.axis.Regular(
        bins=50,
        start=-np.pi,
        stop=np.pi,
        name="jet_phi",
    )
    jet_phi_hist = hist.Hist(
        jet_phi_axis,
        hist.storage.Weight(),
    )
    
    # Axis
    n_bjets = hist.axis.Regular(
        bins=12,
        start=-1,
        stop=11,
        name="number_bjets",
    )
    njet_hist = hist.Hist(
        n_bjets,
        hist.storage.Weight(),
    )


    ttbar_met_axis = hist.axis.Variable(
        edges =[250, 280, 310, 340, 370, 400, 430, 460, 490, 600], #[240, 250, 270, 290, 310, 330, 350, 370, 390, 410, 430, 450, 470, 490, 510],
        name="met",
    )
    met_hist = hist.Hist(
        ttbar_met_axis,
        hist.storage.Weight(),
    )

    met_phi_axis = hist.axis.Regular(
        bins=50,
        start=-np.pi,
        stop=np.pi,
        name="met_phi",
    )
    met_phi_hist = hist.Hist(
        met_phi_axis,
        hist.storage.Weight(),
    )
    
    
    # lepton axes
    lepton_pt_axis = hist.axis.Variable(
        edges=[20, 40,60, 80, 100, 150, 200, 250, 300, 400, 500],
        name="lepton_pt",
    )
    lepton_pt_hist = hist.Hist(
        lepton_pt_axis,
        hist.storage.Weight(),
    )
    
    lepton_eta_axis = hist.axis.Regular(
        bins=24,
        start=-2.4,
        stop=2.4,
        name="lepton_eta",
    )
    lepton_eta_hist = hist.Hist(
        lepton_eta_axis,
        hist.storage.Weight(),
    )
    
    lepton_phi_axis = hist.axis.Regular(
        bins=50,
        start=-np.pi,
        stop=np.pi,
        name="lepton_phi",
    )
    lepton_phi_hist = hist.Hist(
        lepton_phi_axis,
        hist.storage.Weight(),
    )
    
    # lepton + bjet axes
    lepton_bjet_dr_axis = hist.axis.Regular(
        bins=30,
        start=0,
        stop=5,
        name="lepton_bjet_dr",
    )
    lepton_bjet_dr_hist = hist.Hist(
        lepton_bjet_dr_axis,
        hist.storage.Weight(),
    )
    
    lepton_bjet_mass_axis = hist.axis.Variable(
        edges=[40, 75, 100, 125, 150, 175, 200, 300, 500],
        name="lepton_bjet_mass",
    )
    lepton_bjet_mass_hist = hist.Hist(
        lepton_bjet_mass_axis,
        hist.storage.Weight(),
    )
    
    # lepton + missing energy axes
    lepton_met_mass_axis = hist.axis.Variable(
        edges=[40, 75, 100, 125, 150, 175, 200, 300, 500, 800],
        name="lepton_met_mass",
    )
    lepton_met_mass_hist = hist.Hist(
        lepton_met_mass_axis,
        hist.storage.Weight(),
    )
    
    lepton_met_delta_phi_axis = hist.axis.Regular(
        bins=30, start=0, stop=4, name="lepton_met_delta_phi"
    )
    lepton_met_delta_phi_hist = hist.Hist(
        lepton_met_delta_phi_axis,
        hist.storage.Weight(),
    )
    
    # lepton + missing energy + bjet axes
    lepton_met_bjet_mass_axis = hist.axis.Variable(
        edges=[40, 75, 100, 125, 150, 175, 200, 300, 500, 800],
        name="lepton_met_bjet_mass",
    )
    lepton_met_bjet_mass_hist = hist.Hist(
        lepton_met_bjet_mass_axis,
        hist.storage.Weight(),
    )
    
    ##########################
    #####  New plots  ########
    ##########################
    Z_mrec_axis = hist.axis.Regular(
    bins=15,
    start=40,
    stop=100,
    name="Z_mrec",
    )
    Z_mass_hist = hist.Hist(
        Z_mrec_axis,
        hist.storage.Weight(),
    )
    # m_rec_MET axes
    Z_mrec_MET_axis = hist.axis.Regular(
        bins=13,
        start=0,
        stop=260,
        name="Z_mrec_MET",
    )
    Z_mass_MET_hist = hist.Hist(
        Z_mrec_MET_axis,
        hist.storage.Weight(),
    )
    
    # Number of jets
    n_jets_axis = hist.axis.Regular(
        bins=13,
        start=0,
        stop=13,
        name="number_jets",
    )
    n_jets_hist = hist.Hist(
        n_jets_axis,
        hist.storage.Weight(),
    )
    
    # Number of vertices
    n_vertices_axis = hist.axis.Regular(
        bins=10,
        start=0,
        stop=50,
        name="number_vertices",
    )
    n_vertices_hist = hist.Hist(
        n_vertices_axis,
        hist.storage.Weight(),
    )

    # Number of bjets
    n_bjets_axis = hist.axis.Regular(
        bins=5,
        start=0,
        stop=5,
        name="number_bjets",
    )
    n_bjets_hist = hist.Hist(
        n_bjets_axis,
        hist.storage.Weight(),
    )
    
    
    # Z charge axes
    Z_charge_axis = hist.axis.Variable(
        edges=[-2, -1, 0, 1, 2],
        name="Z_charge",
    )
    Z_charge_hist = hist.Hist(
        Z_charge_axis,
        hist.storage.Weight(),
    )

    # lepton axes
    leptonOne_pt_axis = hist.axis.Variable(
        edges=[10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,210],
        name="lepton_one_pt",
    )
    lepton_one_hist = hist.Hist(
        leptonOne_pt_axis,
        hist.storage.Weight(),
    )
    
    
    # lepton axes
    leptonTwo_pt_axis = hist.axis.Variable(
        edges=[10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,210],
        name="lepton_two_pt",
    )
    lepton_two_hist = hist.Hist(
        leptonTwo_pt_axis,
        hist.storage.Weight(),
    )
    

    # times charges
    l1_charge_axis = hist.axis.Variable(
        edges=[-2, -1, 0, 1, 2],
        name="l1_charge",
    )
    l1_charge_hist = hist.Hist(
        l1_charge_axis,
        hist.storage.Weight(),
    )    
    
    
    
    # times charges
    l2_charge_axis = hist.axis.Variable(
        edges=[-2, -1, 0, 1, 2],
        name="l2_charge",
    )
    l2_charge_hist = hist.Hist(
        l2_charge_axis,
        hist.storage.Weight(),
    )   
    
    # met
    Ztoll_met_axis = hist.axis.Variable(
    edges = [10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,210],
    name="met_ztoll",
     )
    Ztoll_met_hist = hist.Hist(
        Ztoll_met_axis,
        hist.storage.Weight(),
    )   
    
    
    # Top
    top_mrec_axis = hist.axis.Variable(
        edges=[120 ,130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300],
        name="top_mrec",
    )
    top_mass_hist = hist.Hist(
        top_mrec_axis,
        hist.storage.Weight(),
    )
    
    # W
    w_mrec_axis = hist.axis.Variable(
        edges=[30 ,40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210],
        name="w_mrec",
    )
    
    W_mass_hist = hist.Hist(
        w_mrec_axis,
        hist.storage.Weight(),
    )   
    
    
    hist_dict = {
        "jet_pt": jet_pt_hist,
        "jet_eta": jet_eta_hist,
        "jet_phi": jet_phi_hist,
        "met": met_hist,
        "met_phi": met_phi_hist,
        "lepton_pt": lepton_pt_hist,
        "lepton_eta": lepton_eta_hist,
        "lepton_phi": lepton_phi_hist,
        "lepton_bjet_dr": lepton_bjet_dr_hist,
        "lepton_bjet_mass": lepton_bjet_mass_hist,
        "lepton_met_mass": lepton_met_mass_hist,
        "lepton_met_delta_phi": lepton_met_delta_phi_hist,
        "lepton_met_bjet_mass": lepton_met_bjet_mass_hist,
        
        "top_mrec": top_mass_hist,
        "w_mrec": W_mass_hist,
        
        "Z_mrec": Z_mass_hist,
        "Z_charge": Z_charge_hist,
        "lepton_one_pt": lepton_one_hist,
        "lepton_two_pt": lepton_two_hist,
        "l1_charge": l1_charge_hist,
        "l2_charge": l2_charge_hist,
        "met_ztoll": Ztoll_met_hist,
        
        "number_vertices": n_vertices_hist,
        "number_bjets": n_bjets_hist,
        "number_jets": n_jets_hist,
    }
    
    histos = {}
    for sample in first_accumulated_histograms:
        
        histos[sample] = copy.deepcopy(hist_dict[feature])
        if first_accumulated_histograms[sample] == None:
            continue
        weight = first_accumulated_histograms[sample]["weights"].value

        fill_args = {feature: first_accumulated_histograms[sample][feature].value}
        histos[sample].fill(
            **fill_args,
            weight=weight
        )
        histos[sample] *= weights[sample]
        
    # group histograms by process
    processed_histograms = group_histograms(histos)
    
    return processed_histograms

def list_plots():
    
    hist_dict = {
        
        "jet": {
            "jet_pt",
            "jet_eta",
            "jet_phi",
        },
        "met": {
            "met",
            "met_phi",            
        },
        "lepton": {
            "lepton_pt",
            "lepton_eta",
            "lepton_phi",                
        },
        "lepton_bjet": {
            "lepton_bjet_dr",
            "lepton_bjet_mass",                 
        },
        "lepton_met": {
            "lepton_met_mass",
            "lepton_met_delta_phi",
            "lepton_met_bjet_mass",                  
        },
        "top_tagger": {
            "top_mrec",
            "w_mrec",               
        },
        "Z": {
            "Z_mrec",
            "Z_charge",  
            "met_ztoll",
        },
        "lepton_one_and_two": {
            "lepton_one_pt",
            "lepton_two_pt",
            "l1_charge",
            "l2_charge",            
        },
        "Numbers": {
            "number_vertices",
            "number_bjets",
            "number_jets",   
        },
    }
    
    return hist_dict

def generate_report(hist_dict):
    # Creamos listas vacías para almacenar las clasificaciones y las variables asociadas
    classifications = []
    variables = []

    # Recorremos el diccionario de histogramas
    for classification, variables_list in hist_dict.items():
        # Por cada clasificación, añadimos la clasificación a la lista de clasificaciones
        # y las variables asociadas a la lista de variables, separadas por comas
        classifications.append(classification)
        variables.append(", ".join(variables_list))

    # Creamos un DataFrame de Pandas con las clasificaciones y las variables asociadas
    report_df = pd.DataFrame({
        "Classification": classifications,
        "Variables": variables
    })

    return report_df

def get_ratio(total_data, total_mc, rax, color="k"):
    ratio = total_data.values() / total_mc.values()
    edges = total_mc.axes.edges[0]
    centers = total_mc.axes.centers[0]

    # ratio
    xerr = edges[1:] - edges[:-1]
    rax.errorbar(
        x=centers,
        y=ratio,
        xerr=xerr / 2,
        fmt=f"{color}o",
        markersize=5,
    )
    numerator = total_data.values()
    denominator = total_mc.values()
    error_down, error_up = ratio_uncertainty(
        num=numerator, denom=denominator, uncertainty_type="poisson-ratio"
    )
    ratio_error_up = ratio + error_up
    ratio_error_down = ratio - error_down

    # plot data/bkg error line
    rax.vlines(centers, ratio_error_down, ratio_error_up, color="k")

    

def plot_hist(processed_hists, feature, lepton_flavor="mu", yratio_limits=None, cms_loc: int = 0, xlimits: tuple = (None, None), title=None, Events_GeV = True):

    mcs = []
    norm_mcs = []
    mcs_labels = []
    for sample in processed_hists:
        if processed_hists[sample] is None: continue
        if sample != "Data":
            mc_hist = processed_hists[sample].project(feature)
            norm_mcs.append(mc_hist / (mc_hist.axes.edges[0][1:] - mc_hist.axes.edges[0][:-1]))
            mcs.append(mc_hist)
            mcs_labels.append(get_sample_map(sample))
    
    total_mc = processor.accumulate(mcs)
    total_data = processed_hists["Data"].project(feature)
    
    fig, (ax, rax) = plt.subplots(
        nrows=2,
        ncols=1,
        figsize=(6, 6),
        tight_layout=True,
        gridspec_kw={"height_ratios": (3, 1)},
        sharex=True,
    )
    
    
    mc_hist, data_hist, error_hist = get_hist_kwargs()
    # Print Histograms:
    print(feature)
    print("MC distributions")
    print(total_mc)
    print("Data distributions")
    print(total_data)
    
    
    if Events_GeV == True:
        # Events/GeV
        hep.histplot(total_data / (total_data.axes.edges[0][1:] - total_data.axes.edges[0][:-1]), ax=ax, label="Data", **data_hist)
        hep.histplot(norm_mcs, ax=ax, label=mcs_labels, **mc_hist)
        
        y_label = "Events/GeV"
        
    else:
        # Events
        hep.histplot(total_data, ax=ax, label="Data", **data_hist)
        hep.histplot(mcs, ax=ax, label=mcs_labels, **mc_hist)
        
        y_label = "Events"
        
    
    # set axes labels and legend
    ncols = 1
    # change legend layout for any distribution with 'eta' or 'phi'
    if ("eta" in feature) or ("phi" in feature):
        ncols = 3
        ylim = ax.get_ylim()[1]
        ax.set_ylim(0, ylim + 0.4 * ylim)
        ax.legend(loc="upper center", ncol=ncols)
    else:
        ax.legend(loc="upper right", ncol=ncols, fontsize=12)
    ax.set(
        xlabel=None,
        ylabel=y_label,
        xlim=xlimits,
    )
    # mc stat
    bkg_error_down, bkg_error_up = poisson_interval(
        values=total_mc.values(), variances=total_mc.variances()
    )
    
    # plot mc uncertainty interval using bar plot
    bar_height = bkg_error_up - bkg_error_down
    bar_centers = total_mc.axes.centers[0]
    bar_width = total_mc.axes.widths[0]
    bar_left_edges = bar_centers - 0.5 * bar_width
    bar_right_edges = bar_centers + 0.5 * bar_width
    ax.bar(
        x=bar_centers,
        height=bar_height,
        width=bar_width,
        bottom=bkg_error_down,
        color='lightgray',  # Color of the bars
        alpha=0.5,  # Transparency of the bars
        label='MC Uncertainty',  # Label of the bars
        hatch='/' * 3,  # Pattern fill with multiple slashes for denser lines
        edgecolor='black',  # Color of the hatch lines
        linewidth = 0,      # 
    )

    
    # ratio
    ratio = total_data.values() / total_mc.values()
    
    edges = total_mc.axes.edges[0]
    centers = total_mc.axes.centers[0]
    xerr = edges[1:] - edges[:-1]
    rax.errorbar(
        x=centers,
        y=ratio,
        xerr=xerr / 2,
        fmt="ko",
        markersize=5,
    )
    numerator = total_data.values()
    denominator = total_mc.values()
    error_down, error_up = ratio_uncertainty(
        num=numerator, denom=denominator, uncertainty_type="poisson-ratio"
    )
    
    ratio_error_up = ratio + error_up
    ratio_error_down = ratio - error_down
    
    # plot data/bkg error line
    rax.vlines(centers, ratio_error_down, ratio_error_up, color="k")
    
    # get bkg/bkg ratio and uncertaity interval
    band_error_down, band_error_up = ratio_uncertainty(
        num=denominator, denom=denominator, uncertainty_type="poisson-ratio"
    )
    yup = np.concatenate([[0], 1 + band_error_up])
    ydown = np.concatenate([[0], 1 - band_error_down])
    ratio_uncertainty_band = rax.fill_between(
        edges,
        yup,
        ydown,
        step="pre",
        **error_hist
    )
    
    # get ratio plot y-limits
    if yratio_limits is None:
        up_limit = np.nanmax(ratio_error_up)
        down_limit = np.nanmin(ratio_error_down)
        scale = 1.1
        yup = scale * up_limit
        ydown = down_limit - scale * (1 - down_limit)

        up_distance = up_limit - 1
        down_distance = down_limit - 1
        if abs(up_distance) > 2 * abs(down_distance):
            ydown = 1 - up_distance
        if yup < 0:
            yup = 1 + scale * max(down_distance, up_distance)  
            
        up_distance = abs(1 - yup)
        down_distance = abs(1 - ydown)
        max_distance = max(down_distance, up_distance)
        ydown, yup = 1-max_distance, 1+max_distance
    else:
        ydown, yup = yratio_limits

    rax.set(
        xlabel= get_label_map(lepton_flavor)[feature],
        ylabel="Data / Back",
        ylim=(ydown, yup),
        facecolor="white",
    )
    # set lumi and CMS text
    hep.cms.lumitext("41.5 fb$^{-1}$ (2017, 13 TeV)", fontsize=12, ax=ax)
    hep.cms.text("Preliminary", loc=cms_loc, ax=ax)
    
    rax.yaxis.label.set_size(10)
    rax.hlines(1, *rax.get_xlim(), color="k", linestyle=":")
    ax.set_title(title)

    # Show the plot
    plt.show()



    

def get_cuts(data_dict):
    
    
    # Obtener todas las muestras disponibles
    samples = list(data_dict.keys())

    # Usar el orden de la primera muestra como referencia para los cortes
    cuts = list(data_dict[samples[0]].keys())
  
    
    # List of samples
    samples = {
        "tt": [],
        "DYJetsToLL": [],
        "WJetsToLNu": [],
        "VV": [],
        "SingleTop": [],
        "Data": [],
    }  
    
    return samples, cuts
